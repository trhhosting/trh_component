import 'package:angular/angular.dart';
import 'dart:html';
import 'dart:async';
import 'dart:convert';
import 'package:articles_logic/logic.dart';
import 'package:trh_component/logic/Articles.dart';
import 'package:trh_component/Story/story.dart';
import 'package:trh_component/SvgIcons/svg_icons.dart';

@Component(
  selector: 'articles',
  templateUrl: 'articles.html',
  styleUrls: [
    'articles.scss.css',
    'alignment.scss.css',
    'icons.scss.css',
  ],
  directives: [
    coreDirectives,
    Story,
    SvgIcons,
  ],
)
class Articles implements OnInit, AfterChanges, AfterViewInit {
  var title = querySelector('#title');
  ArticleData article = ArticleData();
  List<String> articles = <String>[];
  List<Story> stories = <Story>[];

  @ViewChild('articleview')
  Element articleView;
  @ViewChild('work')
  Element work;

  String art = "";

  @ViewChild('article1')
  DivElement article1 = DivElement();

  @ViewChild('article2')
  DivElement article2 = DivElement();

  @ViewChild('article3')
  DivElement article3 = DivElement();

  String icon0 ="/static/icons/gcx_sprite/gcx-sprite.svg#color-palette-1";
  String iconClass0 ="theme-line-9";

  @ViewChild('articletitle0')
  HeadingElement articletitle0;
  @ViewChild('exerpt0')
  ParagraphElement exerpt0 = ParagraphElement();

// <svg class="theme-line-9"><use xlink:href="#luxury-1"></use></svg>
  String icon1 ="/static/icons/gcx_sprite/gcx-sprite.svg#luxury-1";
  String iconClass1 ="theme-line-9";

  @ViewChild('articletitle1')
  HeadingElement articletitle1;
  @ViewChild('exerpt1')
  ParagraphElement exerpt1 = ParagraphElement();
// <svg class="theme-line-9"><use xlink:href="#money-bag-1"></use></svg>
  String icon2 ="/static/icons/gcx_sprite/gcx-sprite.svg#money-bag-1";
  String iconClass2 ="theme-line-9";

  @ViewChild('articletitle2')
  HeadingElement articletitle2;
  @ViewChild('exerpt2')
  ParagraphElement exerpt2 = ParagraphElement();

  List<String> photos = <String>[];
  List<String> photoAlt = <String>[];
String titleArt = "";
String subtitle = "";
String author = "";
String date = "";

  StreamController<dynamic> articleEvents = StreamController<dynamic>();
  String output ="";
  int pt = 0;
  bool move = false; 
  @override
  void ngOnInit() async{
    pt = (window.innerWidth / 24).floor();

    title.text = '';
    output = await getArticlesData();
    articleEvents.add(1);
     if(output != "no article" && output != ""){
      var jsn = json.decode(output);
      article = ArticleData.fromJson(jsn);
      int i = 0;
      titleArt = article.title;

      for (var item in article.articleTitles) {
        i++;
          if(i == 1){
            articletitle0.text = item.title;
            exerpt0.text = item.exerpt;

            // photos.clear();
            // photoAlt.clear();
            // for(var v in item.images){
            //   photoAlt.add(v.imageAlt);
            //   photos.add(v.imageLocation);
            // }
          }
          if(i == 2){
            articletitle1.text = item.title;
            exerpt1.text = item.exerpt;
            // photos.clear();
            // photoAlt.clear();
            // for(var v in item.images){
            //   photoAlt.add(v.imageAlt);
            //   photos.add(v.imageLocation);
            // }
          }
          if(i == 3){
            articletitle2.text = item.title;
            exerpt2.text = item.exerpt;
            // photos.clear();
            // photoAlt.clear();
            // for(var v in item.images){
            //   photoAlt.add(v.imageAlt);
            //   photos.add(v.imageLocation);
            // }

          }

      }

      // print(article.articleTitles[0].title);
        i = 0;
        for (var item in article.articles) {
            i++;
            articles.add(item.response);
            ParagraphElement article = ParagraphElement();
            article.text = item.response;
            if(i == 1){
              article1.append(article);
            }
            if(i == 2){
              article2.append(article);
            }
            if(i == 3){
              article3.append(article);
            }
        }
     }

  }
  @override
  void ngAfterChanges(){
    
  }

  @override
  void ngAfterViewInit(){
    }
  void articleViewText(int aNum) async {
    art = "";
    if(output != "no article" && output != ""){
      var jsn = json.decode(output);
      article = ArticleData.fromJson(jsn);
        var i = 0;
    for (var item in article.articles) {
      if (i  == aNum){
        articles.add(item.response);
        art = item.response;
      }
      i++;
  
    }
    i = 0;
    for (var item in article.articleTitles) {
        photos.clear();
        photoAlt.clear();
        if(i == 0 && i == aNum){
          for(var v in item.images){
            photoAlt.add(v.imageAlt);
            photos.add(v.imageLocation);
          }
          author = item.author;
          date = item.date;
          break;
        }
        if(i == 1 && i == aNum){
          for(var v in item.images){
            photoAlt.add(v.imageAlt);
            photos.add(v.imageLocation);
          }
          author = item.author;
          date = item.date;
          break;
        }
        if(i == 2 && i == aNum){
          for(var v in item.images){
            photoAlt.add(v.imageAlt);
            photos.add(v.imageLocation);
          }
          author = item.author;
          date = item.date;
          break;
        }
        i++;
    }

    articleEvents.add(1);
    }
    if (move){
      pt = (window.outerWidth / 24).floor();
      Element w = querySelector('#work');
      // print(w.scrollHeight);
      window.scrollTo(0, w.scrollHeight + pt * 11);
      move = false;
      return;
    }
    move = true;
  }
}
