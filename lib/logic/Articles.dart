class ArticleData {
  String _title;
  String _subTitle;
  List<ArticleTitles> _articleTitles;
  List<Products> _products;
  List<Articles> _articles;

  ArticleData(
      {String title,
      String subTitle,
      List<ArticleTitles> articleTitles,
      List<Products> products,
      List<Articles> articles}) {
    this._title = title;
    this._subTitle = subTitle;
    this._articleTitles = articleTitles;
    this._products = products;
    this._articles = articles;
  }

  String get title => _title;
  set title(String title) => _title = title;
  String get subTitle => _subTitle;
  set subTitle(String subTitle) => _subTitle = subTitle;
  List<ArticleTitles> get articleTitles => _articleTitles;
  set articleTitles(List<ArticleTitles> articleTitles) =>
      _articleTitles = articleTitles;
  List<Products> get products => _products;
  set products(List<Products> products) => _products = products;
  List<Articles> get articles => _articles;
  set articles(List<Articles> articles) => _articles = articles;

  ArticleData.fromJson(Map<String, dynamic> json) {
    _title = json['title'];
    _subTitle = json['sub_title'];
    if (json['article_titles'] != null) {
      _articleTitles = <ArticleTitles>[];
      json['article_titles'].forEach((v) {
        _articleTitles.add(new ArticleTitles.fromJson(v));
      });
    }
    if (json['products'] != null) {
      _products = <Products>[];
      json['products'].forEach((v) {
        _products.add(new Products.fromJson(v));
      });
    }
    if (json['articles'] != null) {
      _articles = <Articles>[];
      json['articles'].forEach((v) {
        _articles.add(new Articles.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = this._title;
    data['sub_title'] = this._subTitle;
    if (this._articleTitles != null) {
      data['article_titles'] =
          this._articleTitles.map((v) => v.toJson()).toList();
    }
    if (this._products != null) {
      data['products'] = this._products.map((v) => v.toJson()).toList();
    }
    if (this._articles != null) {
      data['articles'] = this._articles.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ArticleTitles {
  String _title;
  String _icon;
  String _author;
  String _date;
  String _exerpt;
  List<Images> _images;

  ArticleTitles(
      {String title,
      String icon,
      String author,
      String date,
      String exerpt,
      List<Images> images}) {
    this._title = title;
    this._icon = icon;
    this._author = author;
    this._date = date;
    this._exerpt = exerpt;
    this._images = images;
  }

  String get title => _title;
  set title(String title) => _title = title;
  String get icon => _icon;
  set icon(String icon) => _icon = icon;
  String get author => _author;
  set author(String author) => _author = author;
  String get date => _date;
  set date(String date) => _date = date;
  String get exerpt => _exerpt;
  set exerpt(String exerpt) => _exerpt = exerpt;
  List<Images> get images => _images;
  set images(List<Images> images) => _images = images;

  ArticleTitles.fromJson(Map<String, dynamic> json) {
    _title = json['title'];
    _icon = json['icon'];
    _author = json['author'];
    _date = json['date'];
    _exerpt = json['exerpt'];
    if (json['images'] != null) {
      _images = <Images>[];
      json['images'].forEach((v) {
        _images.add(new Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = this._title;
    data['icon'] = this._icon;
    data['author'] = this._author;
    data['date'] = this._date;
    data['exerpt'] = this._exerpt;
    if (this._images != null) {
      data['images'] = this._images.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Images {
  String _imageLocation;
  String _imageAlt;

  Images({String imageLocation, String imageAlt}) {
    this._imageLocation = imageLocation;
    this._imageAlt = imageAlt;
  }

  String get imageLocation => _imageLocation;
  set imageLocation(String imageLocation) => _imageLocation = imageLocation;
  String get imageAlt => _imageAlt;
  set imageAlt(String imageAlt) => _imageAlt = imageAlt;

  Images.fromJson(Map<String, dynamic> json) {
    _imageLocation = json['image_location'];
    _imageAlt = json['image_alt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['image_location'] = this._imageLocation;
    data['image_alt'] = this._imageAlt;
    return data;
  }
}

class Products {
  String _title;
  String _subTitle;
  String _image;
  String _affilateLink;

  Products({String title, String subTitle, String image, String affilateLink}) {
    this._title = title;
    this._subTitle = subTitle;
    this._image = image;
    this._affilateLink = affilateLink;
  }

  String get title => _title;
  set title(String title) => _title = title;
  String get subTitle => _subTitle;
  set subTitle(String subTitle) => _subTitle = subTitle;
  String get image => _image;
  set image(String image) => _image = image;
  String get affilateLink => _affilateLink;
  set affilateLink(String affilateLink) => _affilateLink = affilateLink;

  Products.fromJson(Map<String, dynamic> json) {
    _title = json['title'];
    _subTitle = json['sub_title'];
    _image = json['image'];
    _affilateLink = json['affilate_link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = this._title;
    data['sub_title'] = this._subTitle;
    data['image'] = this._image;
    data['affilate_link'] = this._affilateLink;
    return data;
  }
}

class Articles {
  String _apiRequestsAvailable;
  String _apiRequestsMade;
  String _confidenceLevel;
  String _protectedTerms;
  String _response;
  String _status;

  Articles(
      {String apiRequestsAvailable,
      String apiRequestsMade,
      String confidenceLevel,
      String protectedTerms,
      String response,
      String status}) {
    this._apiRequestsAvailable = apiRequestsAvailable;
    this._apiRequestsMade = apiRequestsMade;
    this._confidenceLevel = confidenceLevel;
    this._protectedTerms = protectedTerms;
    this._response = response;
    this._status = status;
  }

  String get apiRequestsAvailable => _apiRequestsAvailable;
  set apiRequestsAvailable(String apiRequestsAvailable) =>
      _apiRequestsAvailable = apiRequestsAvailable;
  String get apiRequestsMade => _apiRequestsMade;
  set apiRequestsMade(String apiRequestsMade) =>
      _apiRequestsMade = apiRequestsMade;
  String get confidenceLevel => _confidenceLevel;
  set confidenceLevel(String confidenceLevel) =>
      _confidenceLevel = confidenceLevel;
  String get protectedTerms => _protectedTerms;
  set protectedTerms(String protectedTerms) => _protectedTerms = protectedTerms;
  String get response => _response;
  set response(String response) => _response = response;
  String get status => _status;
  set status(String status) => _status = status;

  Articles.fromJson(Map<String, dynamic> json) {
    _apiRequestsAvailable = json['api_requests_available'];
    _apiRequestsMade = json['api_requests_made'];
    _confidenceLevel = json['confidence_level'];
    _protectedTerms = json['protected_terms'];
    _response = json['response'];
    _status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['api_requests_available'] = this._apiRequestsAvailable;
    data['api_requests_made'] = this._apiRequestsMade;
    data['confidence_level'] = this._confidenceLevel;
    data['protected_terms'] = this._protectedTerms;
    data['response'] = this._response;
    data['status'] = this._status;
    return data;
  }
}
