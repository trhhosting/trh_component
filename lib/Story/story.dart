import 'dart:html';

import 'package:angular/angular.dart';
import 'dart:async';

@Component(
  selector: 'story',
  templateUrl: 'story.html',
  styleUrls: [
    'story.scss.css',
  ],
  directives: [
    coreDirectives,
  ]
)
class Story implements OnInit, AfterViewInit {
  @Input('article-events')
  StreamController<dynamic> articleEvents = StreamController();

  @Input('article')
  String article = "";
  @Input('title')
  String title = "";
  @Input('subtitle')
  String subtitle = "";
  @Input('author')
  String author = "";
  @Input('date')
  String date = "";
  @Input('photos')
  List<String> photos = <String>[];
  @Input('photoAlt')
  List<String> photoAlt = <String>[];


  @ViewChild('articletext')
  DivElement articletext = DivElement();

  @ViewChild('photomain')
  ImageElement photomain = ImageElement();

  List<String> pargraph = <String>[];
  int pt = 0;
  @override 
  void ngOnInit(){
    pt = (window.innerWidth / 24).floor();
    
  }
  bool artText = false;
  List<Element> rmItems = <Element>[];
  String mainphoto = "";
  @override
  void ngAfterViewInit() {
      articleEvents.stream.listen((e){
        if (article.isNotEmpty){
          articleFormatter(article);
          // print("article data");
        } else{
          print("no data ${article.length}");
        }
      });
      
  }
  void articleFormatter(String data){
      pt = (window.innerWidth / 24).floor();

      pargraph = data.split('\n');
      if(rmItems.isNotEmpty){
        for (var v in rmItems){
          articletext.children.remove(v);
        }
        rmItems.clear();
        // print("rmItems.clear()");
      }
      for (var item in pargraph) {
        ParagraphElement p = ParagraphElement();
        p.text = item;
        rmItems.add(p);
        // print("ParagraphElement p = ParagraphElement() ${p.text}");        
      }
      int i = 0;
      int imgCount = photos.length;
      // print(imgCount);
      for (var v in rmItems){
        i++;
        if(i == 2){
          if (imgCount > 0){
            if (imgCount == 1){
                photomain.src = photos[0];
                mainphoto = photos[0];
                break;
            }
            int w = pt * 10;
            v.appendHtml('''
            <div class="col-lg-3">
                <div class="single_work">
                    <div class="work_image">
                        <img width="${w}px" src="${photos[imgCount -1]}" alt="${photoAlt[imgCount -1]}">
                    </div>
                    <div class="work_content">
                        <a href="#" class="arrow"><i class="lni lni-chevron-right"></i></a>
                        <p><a href="#">${title}</a></p>
                        <p>${subtitle}</p>
                    </div>
                </div> <!-- single work -->
            </div>
            ''');
          imgCount--;
          }
          i = 0;
        }
        articletext.append(v);
        // print("articletext.append(${v.text})");
      }
      article = "";
      photos.clear();
      photoAlt.clear();
  }

}
