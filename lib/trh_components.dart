// Copyright (c) 2016, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

library trh_component;

import 'Articles/articles.dart';
import 'SvgIcons/svg_icons.dart';
// export 'Articles/articles.dart';
// export 'SvgIcons/svg_icons.dart';



/// A convenience list of all Directives exposed by this package.
// @Deprecated('List the directives used by your app for smaller code size.')
const List<dynamic> trhDirectives = [
  Articles,
  SvgIcons,
];

/// A convenience list of all providers exposed by this package.
// const List<dynamic> materialProviders = [
// ];
